using Microsoft.AspNetCore.Mvc;
using Produtos.Models;

namespace Produtos.Controllers;

[ApiController]
[Route("[controller]")]
public class ProdutoController: ControllerBase
{
    private static readonly Produto[] Produtos = new[]
    {
        new Produto() {Nome="Sabonete", Valor=9.90M},
        new Produto() {Nome="Libigel", Valor=29.50M},
        new Produto() {Nome="Raspbarry Pi", Valor=396.0M}
    };

    
    [HttpGet]
    public IEnumerable<Produto> GetProdutos()
    {
        return Produtos;
    }
}
