using Microsoft.AspNetCore.Mvc;

namespace Reembolso.Controllers;

[ApiController]
[Route("[controller]")]
public class ReembolsoController: ControllerBase
{
    [HttpGet]
    public Object TentaReembolso(decimal valor)
    {
            return new {Valor=valor,Conseguio=Random.Shared.Next() % 2 == 0};
    }
}
