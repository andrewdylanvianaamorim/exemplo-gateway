using Microsoft.AspNetCore.Mvc;
using Frontend.Models;
using System.Text.Json;

public class ProdutosController: Controller
{
    private HttpClient http;

    public ProdutosController()
    {
        http = new();
    }

    public IActionResult Index()
    {
        
        IEnumerable<Produto> produtos = http.GetFromJsonAsync<IEnumerable<Produto>>("http://localhost:5237/produto").Result!;
        Console.WriteLine("Nome: " + produtos.First().Nome);
        return View(produtos);
    }

}