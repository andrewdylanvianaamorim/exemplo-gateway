using Microsoft.AspNetCore.Mvc;
using Frontend.Models;
namespace Frontend.Controllers;

public class ReembolsoController : Controller
{
    private HttpClient http;

    public ReembolsoController() 
    {
        http = new HttpClient();
    }

    public IActionResult Index()
    {

        var reembolso = http.GetFromJsonAsync<Reembolso>("http://localhost:5116/Reembolso?valor=20").Result;
        return View(reembolso);
    }
}